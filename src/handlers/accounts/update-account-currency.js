/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const TransactionRepository = require('../../repositories/transaction-repository');
  const TransactionForm = require('../../forms/transaction-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  const transactionRepository = TransactionRepository.sharedInstance;
  const transactionForm = new TransactionForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: {
          id: isNaN(parseInt(req.params.id)) ? req.params.id : parseInt(req.params.id),
          currency: req.body.currency,
          rate_base: isNaN(parseInt(req.body.rateBase)) ? req.body.rateBase : parseInt(req.body.rateBase),
          rate_exponent: isNaN(parseInt(req.body.rateExponent)) ? req.body.rateExponent : parseInt(req.body.rateExponent),
          tags: req.body.tags,
          attributes: req.body.attributes || {},
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = accountForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      accountRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      accountRepository.findByPk(context.params.id, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('account', context.params.id));
        }

        context.recordToBeUpdated = foundRecord;

        next();
      });
    },

    function checkAccountIntegrity({context}, _, next) {
      if (!AccountForm.isAccountSignatureValid(context.recordToBeUpdated, context.recordToBeUpdated.signature)) {
        return next(new Errors.CorruptAccountError('account', {
          account: accountForm.toSerializable(context.recordToBeUpdated)
        }));
      }

      next();
    },

    function calculateNewAccountBalance({context}, _, next) {
      context.appliedRateMultiplier = (context.params.rate_base * Math.pow(10, context.params.rate_exponent));
      context.previousBalance = context.recordToBeUpdated.balance;
      context.previousCurrency = context.recordToBeUpdated.currency;
      context.newBalance = Math.floor(context.recordToBeUpdated.balance * context.appliedRateMultiplier);

      next();
    },

    function updateRecord({context}, _, next) {
      accountRepository.updateByPk(context.params.id, {
        balance: context.newBalance,
        currency: context.params.currency,
        dt_updated: context.params.dt_updated
      }, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('account', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function signAccountRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.updatedRecord);

      accountRepository.updateByPk(context.params.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('account', context.params.id));
        }

        context.signedRecord = record;

        next();
      });
    },

    function createTransaction({context}, _, next) {
      transactionRepository.insert({
        tags: context.params.tags,
        sent_amount: context.previousBalance,
        sent_currency: context.previousCurrency,
        received_amount: context.newBalance,
        received_currency: context.params.currency,
        sender: context.signedRecord.id,
        receiver: context.signedRecord.id,
        signature: 'UNSET',
        attributes: {
          context: context.params.attributes,
          system: {
            previousSenderBalance: context.previousBalance,
            newSenderBalance: context.newBalance,
            previousReceiverBalance: context.previousBalance,
            newReceiverBalance: context.newBalance,
            appliedRateMultiplier: context.appliedRateMultiplier,
            rateBase: context.params.rate_base,
            rateExponent: context.params.rate_exponent,
            tag: 'UPDATE_CURRENCY'
          }
        },
        dt_created: context.params.dt_updated
      }, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdTransaction = records[0];

        next();
      });
    },

    function signTransactionRecord({context}, _, next) {
      let signature = TransactionForm.computeTransactionSignature(context.createdTransaction);

      transactionRepository.updateByPk(context.createdTransaction.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('transaction', context.createdTransaction.id));
        }

        context.signedTransactionRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      accountRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        account: accountForm.toSerializable(context.signedRecord),
        transaction: transactionForm.toSerializable(context.signedTransactionRecord)
      });

      next();
    }];
}());