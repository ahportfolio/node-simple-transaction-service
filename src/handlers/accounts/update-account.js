/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: {
          id: isNaN(parseInt(req.params.id)) ? req.params.id : parseInt(req.params.id),
          attributes: req.body.attributes,
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = accountForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      accountRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let updateValues = Util.omit(context.params, ['id']);

      accountRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('account', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function signRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.updatedRecord);

      accountRepository.updateByPk(context.params.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('account', context.params.id));
        }

        context.signedRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      accountRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(accountForm.toSerializable(context.signedRecord));

      next();
    }];
}());