/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const TransactionRepository = require('../../repositories/transaction-repository');
  const TransactionForm = require('../../forms/transaction-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  const transactionRepository = TransactionRepository.sharedInstance;
  const transactionForm = new TransactionForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: Util.omitBy({
          ids: Array.isArray(req.query.ids) ? req.query.ids.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : req.query.ids,
          _truncate: req.query._truncate
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = accountForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      accountRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function removeRecords({context}, _, next) {
      let query = Util.pick(context.params, []);
      if (context.params.ids) {
        query.id = {$in: context.params.ids};
      }

      if (Object.keys(query).length === 0 && context.params._truncate !== 'YES') {
        return next(new Errors.InvalidRequestError({
          error: 'At least one criteria must be specified'
        }));
      }

      accountRepository.find(query, undefined, undefined, undefined, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        let idsToRemove = records.map(record => record.id);

        accountRepository.remove({id: {$in: idsToRemove}}, context.txn, (err, records) => {
          next.ifError(err && new Errors.UnexpectedError(err));

          context.idsRemoved = idsToRemove;
          context.removedRecords = records;

          next();
        });
      });
    },

    function cleanUpOrphanedTransactions({context}, _, next) {
      transactionRepository.remove({
        sender: null,
        receiver: null
      }, context.txn, (err, removedTransactions) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.removedTransactionRecords = removedTransactions;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      accountRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        totalRecords: context.removedRecords.length,
        records: context.removedRecords.map(accountForm.toSerializable.bind(accountForm)),
        removedTransactions: context.removedTransactionRecords.map(transactionForm.toSerializable.bind(transactionForm)),

        ids: context.params.ids || null,
        _truncate: context.params._truncate
      });

      next();
    }];
}());