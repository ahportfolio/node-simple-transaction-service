/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: {
          currency: req.body.currency,
          attributes: req.body.attributes || {},
          dt_created: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = accountForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      accountRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function createRecord({context}, _, next) {
      accountRepository.insert(Object.assign({}, context.params, {
        balance: 0,
        signature: 'UNSET'
      }), context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdRecord = records[0];

        next();
      });
    },

    function signRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.createdRecord);

      accountRepository.updateByPk(context.createdRecord.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('account', context.createdRecord.id));
        }

        context.signedRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      accountRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(accountForm.toSerializable(context.signedRecord));

      next();
    }];
}());