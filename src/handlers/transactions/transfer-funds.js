/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const TransactionRepository = require('../../repositories/transaction-repository');
  const TransactionForm = require('../../forms/transaction-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  const transactionRepository = TransactionRepository.sharedInstance;
  const transactionForm = new TransactionForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: {
          amount: isNaN(parseInt(req.body.amount)) ? req.body.amount : parseInt(req.body.amount),
          sender: isNaN(parseInt(req.body.sender)) ? req.body.sender : parseInt(req.body.sender),
          receiver: isNaN(parseInt(req.body.receiver)) ? req.body.receiver : parseInt(req.body.receiver),
          rate_base: isNaN(parseInt(req.body.rateBase)) ? req.body.rateBase : parseInt(req.body.rateBase),
          rate_exponent: isNaN(parseInt(req.body.rateExponent)) ? req.body.rateExponent : parseInt(req.body.rateExponent),
          tags: req.body.tags,
          attributes: req.body.attributes || {},
          dt_created: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = transactionForm.validate(context.params, ['rate_base', 'rate_exponent'], true);

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      transactionRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function fetchSenderRecord({context}, _, next) {
      accountRepository.findByPk(context.params.sender, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('sender account', context.params.sender));
        }

        context.senderRecord = foundRecord;

        next();
      });
    },

    function fetchReceiverRecord({context}, _, next) {
      accountRepository.findByPk(context.params.receiver, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.receiver));
        }

        context.receiverRecord = foundRecord;

        next();
      });
    },

    function checkSenderAccountIntegrity({context}, _, next) {
      if (!AccountForm.isAccountSignatureValid(context.senderRecord, context.senderRecord.signature)) {
        return next(new Errors.CorruptAccountError('sender account', {
          sender: accountForm.toSerializable(context.senderRecord)
        }));
      }

      next();
    },

    function checkReceiverAccountIntegrity({context}, _, next) {
      if (!AccountForm.isAccountSignatureValid(context.receiverRecord, context.receiverRecord.signature)) {
        return next(new Errors.CorruptAccountError('receiver account', {
          receiver: accountForm.toSerializable(context.receiverRecord)
        }));
      }

      next();
    },

    function checkSameSenderAndReceiver({context}, _, next) {
      if (context.senderRecord.id === context.receiverRecord.id) {
        return next(new Errors.InvalidRequestError({
          error: 'The sender id cannot be the same as the receiver id'
        }));
      }

      next();
    },

    function checkCrossCurrencyTransfer({context}, _, next) {
      if (context.senderRecord.currency !== context.receiverRecord.currency
          && (context.params.rate_base === undefined || context.params.rate_exponent === undefined)) {
        return next(new Errors.InvalidRequestError({
          sender: accountForm.toSerializable(context.senderRecord),
          receiver: accountForm.toSerializable(context.receiverRecord),
          error: 'The accounts are not the same currency so an explicit rate base and rate exponent must be ' +
          'specified to be used to execute the transaction'
        }));
      }

      next();
    },

    function checkSenderSufficientFunds({context}, _, next) {
      if (context.senderRecord.balance < context.params.amount) {
        return next(new Errors.InsufficientFundsError({
          sender: accountForm.toSerializable(context.senderRecord),
          amount: context.params.amount
        }));
      }

      next();
    },

    function calculateUpdatedBalances({context}, _, next) {
      if (context.senderRecord.currency !== context.receiverRecord.currency) {
        context.appliedRateMultiplier = (context.params.rate_base * Math.pow(10, context.params.rate_exponent));
      } else {
        context.appliedRateMultiplier = 1;
      }

      context.previousSenderBalance = context.senderRecord.balance;
      context.previousReceiverBalance = context.receiverRecord.balance;

      context.newSenderBalance = context.senderRecord.balance - context.params.amount;

      context.amountToReceive = Math.floor(context.params.amount * context.appliedRateMultiplier);
      context.newReceiverBalance = context.receiverRecord.balance + context.amountToReceive;

      next();
    },

    function updateSenderRecord({context}, _, next) {
      accountRepository.updateByPk(context.params.sender, {
        balance: context.newSenderBalance,
        dt_updated: context.params.dt_created
      }, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('sender account', context.params.sender));
        }

        context.updatedSenderRecord = record;

        next();
      });
    },

    function updateReceiverRecord({context}, _, next) {
      accountRepository.updateByPk(context.params.receiver, {
        balance: context.newReceiverBalance,
        dt_updated: context.params.dt_created
      }, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.sender));
        }

        context.updatedReceiverRecord = record;

        next();
      });
    },

    function signSenderAccountRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.updatedSenderRecord);

      accountRepository.updateByPk(context.params.sender, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('sender account', context.params.sender));
        }

        context.signedSenderRecord = record;

        next();
      });
    },

    function signReceiverAccountRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.updatedReceiverRecord);

      accountRepository.updateByPk(context.params.receiver, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.receiver));
        }

        context.signedReceiverRecord = record;

        next();
      });
    },

    function createTransaction({context}, _, next) {
      transactionRepository.insert({
        tags: context.params.tags,
        sent_amount: context.params.amount,
        sent_currency: context.senderRecord.currency,
        received_amount: context.amountToReceive,
        received_currency: context.receiverRecord.currency,
        sender: context.senderRecord.id,
        receiver: context.receiverRecord.id,
        signature: 'UNSET',
        attributes: {
          context: context.params.attributes,
          system: {
            originalSenderId: context.senderRecord.id,
            originalReceiverId: context.receiverRecord.id,
            previousSenderBalance: context.previousSenderBalance,
            newSenderBalance: context.newSenderBalance,
            previousReceiverBalance: context.previousReceiverBalance,
            newReceiverBalance: context.newReceiverBalance,
            appliedRateMultiplier: context.appliedRateMultiplier,
            rateBase: context.params.rate_base || null,
            rateExponent: context.params.rate_exponent || null,
            tag: 'TRANSFER_FUNDS'
          }
        },
        dt_created: context.params.dt_created
      }, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdTransaction = records[0];

        next();
      });
    },

    function signTransactionRecord({context}, _, next) {
      let signature = TransactionForm.computeTransactionSignature(context.createdTransaction);

      transactionRepository.updateByPk(context.createdTransaction.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('transaction', context.createdTransaction.id));
        }

        context.signedTransactionRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      transactionRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        sender: accountForm.toSerializable(context.signedSenderRecord),
        receiver: accountForm.toSerializable(context.signedReceiverRecord),
        transaction: transactionForm.toSerializable(context.signedTransactionRecord)
      });

      next();
    }];
}());