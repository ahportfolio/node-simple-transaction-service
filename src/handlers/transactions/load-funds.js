/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Errors = require('../../errors/api-errors');

  const AccountRepository = require('../../repositories/account-repository');
  const AccountForm = require('../../forms/account-form');

  const TransactionRepository = require('../../repositories/transaction-repository');
  const TransactionForm = require('../../forms/transaction-form');

  const accountRepository = AccountRepository.sharedInstance;
  const accountForm = new AccountForm();

  const transactionRepository = TransactionRepository.sharedInstance;
  const transactionForm = new TransactionForm();

  module.exports = [
    function extractParameters(req, _, next) {
      req.context = {
        params: {
          amount: isNaN(parseInt(req.body.amount)) ? req.body.amount : parseInt(req.body.amount),
          receiver: isNaN(parseInt(req.body.receiver)) ? req.body.receiver : parseInt(req.body.receiver),
          tags: req.body.tags,
          attributes: req.body.attributes || {},
          dt_created: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = transactionForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      transactionRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function fetchReceiverRecord({context}, _, next) {
      accountRepository.findByPk(context.params.receiver, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.receiver));
        }

        context.receiverRecord = foundRecord;

        next();
      });
    },

    function checkReceiverAccountIntegrity({context}, _, next) {
      if (!AccountForm.isAccountSignatureValid(context.receiverRecord, context.receiverRecord.signature)) {
        return next(new Errors.CorruptAccountError('receiver account', {
          receiver: accountForm.toSerializable(context.receiverRecord)
        }));
      }

      next();
    },

    function calculateUpdatedBalances({context}, _, next) {
      context.appliedRateMultiplier = 1;

      context.previousReceiverBalance = context.receiverRecord.balance;

      context.amountToReceive = Math.floor(context.params.amount * context.appliedRateMultiplier);
      context.newReceiverBalance = context.receiverRecord.balance + context.amountToReceive;

      next();
    },

    function updateReceiverRecord({context}, _, next) {
      accountRepository.updateByPk(context.params.receiver, {
        balance: context.newReceiverBalance,
        dt_updated: context.params.dt_created
      }, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.sender));
        }

        context.updatedReceiverRecord = record;

        next();
      });
    },

    function signReceiverAccountRecord({context}, _, next) {
      let signature = AccountForm.computeAccountSignature(context.updatedReceiverRecord);

      accountRepository.updateByPk(context.params.receiver, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('receiver account', context.params.receiver));
        }

        context.signedReceiverRecord = record;

        next();
      });
    },

    function createTransaction({context}, _, next) {
      transactionRepository.insert({
        tags: context.params.tags,
        sent_amount: null,
        sent_currency: null,
        received_amount: context.amountToReceive,
        received_currency: context.receiverRecord.currency,
        sender: null,
        receiver: context.receiverRecord.id,
        signature: 'UNSET',
        attributes: {
          context: context.params.attributes,
          system: {
            originalSenderId: null,
            originalReceiverId: context.receiverRecord.id,
            previousSenderBalance: null,
            newSenderBalance: null,
            previousReceiverBalance: context.previousReceiverBalance,
            newReceiverBalance: context.newReceiverBalance,
            appliedRateMultiplier: context.appliedRateMultiplier,
            rateBase: null,
            rateExponent: null,
            tag: 'LOAD_FUNDS'
          }
        },
        dt_created: context.params.dt_created
      }, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdTransaction = records[0];

        next();
      });
    },

    function signTransactionRecord({context}, _, next) {
      let signature = TransactionForm.computeTransactionSignature(context.createdTransaction);

      transactionRepository.updateByPk(context.createdTransaction.id, {signature}, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('transaction', context.createdTransaction.id));
        }

        context.signedTransactionRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      transactionRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        sender: null,
        receiver: accountForm.toSerializable(context.signedReceiverRecord),
        transaction: transactionForm.toSerializable(context.signedTransactionRecord)
      });

      next();
    }];
}());