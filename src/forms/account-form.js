/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Fs = require('fs');

  const DataForm = require('js-data-form');
  const Crypto = require('node-crypto-util');

  const APP_PRIVATE_KEY = (process.env.APP_KEY_PATH && Fs.readFileSync(process.env.APP_KEY_PATH, {encoding: 'utf8'}));
  if (!APP_PRIVATE_KEY) {
    throw new Error("A valid 'APP_KEY_PATH' must be provided");
  }

  const SIGNATORY_FIELDS = Object.assign({}, ...[
    'id', 'balance', 'currency', 'attributes', 'dt_created', 'dt_updated'
  ].map(f=>({[f]: (v)=>v[f]})));

  class EntityForm extends DataForm {
    constructor() {
      super({
        fields: {
          offset: {
            type: 'integer',
            minimum: 0
          },
          limit: {
            type: 'integer',
            minimum: 1
          },
          id: {
            type: 'integer',
            minimum: 1
          },
          _truncate: {},
          ids: {
            type: 'array',
            minItems: 1,
            items: {
              type: 'integer',
              minimum: 1
            }
          },
          tags: {
            type: 'array',
            items: {
              type: 'string',
              minLength: 1,
              maxLength: 55
            }
          },
          currency: {
            type: 'string',
            minLength: 1,
            maxLength: 55
          },
          rate_base: {
            type: 'integer',
            minimum: 1
          },
          rate_exponent: {
            type: 'integer',
            maximum: 0
          },
          attributes: {
            type: 'object'
          },
          dt_created: {
            type: 'integer',
            minimum: 1
          },
          dt_updated: {
            type: 'integer',
            minimum: 1
          }
        }
      });
    }

    static computeAccountSignature(account) {
      return Crypto.Signature.signObject(
          account, SIGNATORY_FIELDS, APP_PRIVATE_KEY, Date.now(),
          'base64', 'sha256', 'account_object_signature'
      );
    }

    static isAccountSignatureValid(account, signature) {
      return Crypto.Signature.verifyObjectSignature(
          account, SIGNATORY_FIELDS, [], APP_PRIVATE_KEY, signature, -1,
          'base64', 'sha256', 'account_object_signature'
      )
    }

    static analyzeAccountSignatureValid(account, signature) {
      return Crypto.Signature.analyzeObjectSignature(
          account, SIGNATORY_FIELDS, [], APP_PRIVATE_KEY, signature, -1,
          'base64', 'sha256', 'account_object_signature'
      )
    }

    toSerializable(data) {
      return Object.assign(...[
        'id', 'balance', 'currency', 'attributes', 'dt_created', 'dt_updated'
      ].map(k=>({[k]: data[k]})), {
        signature: Crypto.Digests.md5(data.signature, 'base64')
      }, {
        isValid: EntityForm.isAccountSignatureValid(data, data.signature)
      });
    }
  }

  module.exports = EntityForm;
}());