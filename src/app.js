/**
 * Created by Aston Hamilton
 */
(function () {

  /**
   *
   * @type {Server}
   */
  const server = require('./_server');
  const logger = server.log.child({stream: process.stdout});

  server.listen(process.env.NODE_PORT, function () {
    logger.info('Application listening on: %s', server.url);
  });

  module.exports = server;
}());