/**
 * Created by Aston Hamilton
 */

(function () {
  'use strict';

  const Restify = require('restify');

  class APIError extends Restify.RestError {
    constructor(constructorOpt, message, data = {}) {
      super({
        statusCode: 422,
        message: message,
        restCode: constructorOpt.name,
        constructorOpt: constructorOpt
      });

      //noinspection JSUnresolvedVariable
      this.body.data = data;
    }
  }

  class ParameterValidationError extends APIError {
    constructor(parameters, errors, err = undefined) {
      super(ParameterValidationError, 'The parameters are not valid', {
        parameters, errors,
        error: (err ? ': ' + String(err) : undefined)
      });
    }
  }

  class InvalidRequestError extends APIError {
    constructor(data, err = undefined) {
      super(InvalidRequestError, 'The request is not valid', Object.assign({
        error: (err ? ': ' + String(err) : undefined)
      }, data));
    }
  }

  class CorruptAccountError extends APIError {
    constructor(identifier, data, err = undefined) {
      super(CorruptAccountError, `The ${identifier} is not corrupt`, Object.assign({
        error: (err ? ': ' + String(err) : undefined)
      }, data));
    }
  }

  class UnexpectedError extends APIError {
    constructor(err) {
      super(UnexpectedError, 'An unexpected error has occurred', {
        error: String(err)
      });
    }
  }

  class UnknownIdentifierError extends APIError {
    constructor(targetName, identifier, err = undefined) {
      super(UnknownIdentifierError, `No ${targetName} matched the identifier`, {
        identifier: identifier,
        error: (err ? ': ' + String(err) : undefined)
      });
    }
  }

  class InsufficientFundsError extends APIError {
    constructor(data, err = undefined) {
      super(InsufficientFundsError, `The account does not have enough funds to process the transaction`, Object.assign({
        error: (err ? ': ' + String(err) : undefined)
      }, data));
    }
  }

  module.exports = {
    APIError,
    UnknownIdentifierError,
    UnexpectedError,
    ParameterValidationError,
    InvalidRequestError,
    CorruptAccountError,
    InsufficientFundsError
  };
}());
