/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const EntityRepository = require('node-dbal-pg');

  var INSTANCE = null;

  class Repository extends EntityRepository {
    constructor() {
      super({
        username: process.env.APP_DB_USERNAME,
        password: process.env.APP_DB_PASSWORD,
        host: process.env.APP_DB_HOST,
        port: process.env.APP_DB_PORT,
        database: process.env.APP_DB_NAME
      }, {
        table: 'transaction_manager_accounts',
        pk: 'id',
        converters: {
          attributes: EntityRepository.converters.JSON_IN
        }
      })
    }

    static get sharedInstance() {
      if (INSTANCE === null) {
        INSTANCE = new Repository();

        INSTANCE.client.enableDebugging();
      }

      return INSTANCE;
    }
  }

  module.exports = Repository;
}());