/**
 * Created by Aston Hamilton
 */
(function () {
  const Restify = require('restify');

  const server = Restify.createServer({name: process.env.APP_NAME});
  const logger = server.log.child({stream: process.stdout});

  server.use(Restify.acceptParser(server.acceptable));
  server.use(Restify.CORS());
  server.use(Restify.queryParser({mapParams: false}));
  server.use(Restify.bodyParser({mapParams: false}));
  server.use(Restify.requestLogger());
  server.use(Restify.gzipResponse());
  server.use(Restify.conditionalRequest());

  // register handlers
  server.post('/accounts', require('./handlers/accounts/create-account'));
  server.get('/accounts', require('./handlers/accounts/get-accounts'));
  server.put('/accounts/:id', require('./handlers/accounts/update-account'));
  server.put('/accounts/:id/currency', require('./handlers/accounts/update-account-currency'));
  server.del('/accounts', require('./handlers/accounts/remove-accounts'));

  server.post('/transactions', require('./handlers/transactions/transfer-funds'));
  server.post('/transactions/load', require('./handlers/transactions/load-funds'));
  server.post('/transactions/unload', require('./handlers/transactions/unload-funds'));
  server.get('/transactions', require('./handlers/transactions/get-transactions'));

  server.on('after', Restify.auditLogger({log: logger, body: true}));

  server.on('uncaughtException', function (req, res, route, error) {
    req.log.error({err: error});

    res.send(new Restify.InternalError(error, error.message || 'Unexpected application error'));

    setTimeout(() => {
      server.close(() => {
        logger.info('Encountered an unhandled error so shutting down the server');
        process.exit(1);
      }, 2000);
    });
  });

  /**
   *
   * @type {Server}
   */
  module.exports = server;
}());