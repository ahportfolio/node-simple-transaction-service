CREATE TABLE transaction_manager_accounts
(
  id         SERIAL  NOT NULL,
  balance    INTEGER NOT NULL,
  currency   TEXT    NOT NULL,
  attributes JSONB   NOT NULL,
  signature  TEXT    NOT NULL,
  dt_created BIGINT  NOT NULL,
  dt_updated BIGINT,
  CONSTRAINT transaction_manager_accounts_pkey PRIMARY KEY (id)
);

CREATE TABLE transaction_manager_transactions
(
  id                SERIAL NOT NULL,
  sent_amount       INTEGER,
  sent_currency     TEXT,
  sender            INTEGER,
  received_amount   INTEGER,
  received_currency TEXT,
  receiver          INTEGER,
  attributes        JSONB  NOT NULL,
  signature         TEXT   NOT NULL,
  dt_created        BIGINT NOT NULL,
  tags              JSONB  NOT NULL,
  CONSTRAINT transaction_manager_transactions_pkey PRIMARY KEY (id),
  CONSTRAINT transaction_mananger_transactions_receiver_fkey FOREIGN KEY (receiver)
  REFERENCES transaction_manager_accounts (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE SET NULL,
  CONSTRAINT transaction_mananger_transactions_sender_fkey FOREIGN KEY (sender)
  REFERENCES transaction_manager_accounts (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE SET NULL
);

CREATE INDEX transaction_manager_transactions_receiver
ON transaction_manager_transactions (receiver);

CREATE INDEX transaction_manager_transactions_sender
ON transaction_manager_transactions (sender);

CREATE INDEX transaction_manager_transactions_tags
ON transaction_manager_transactions
USING GIN (tags);

