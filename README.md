# app-simple-transaction-service

This application manages transactional accounts and their associated transaction records.
It was built using NodeJS to expose an HTTP/JSON interface as the primary means of data manipulation.

## Configuration

| Type |  Key  | Comments |
| ---- | :---: | -------- |
| ENV  | NODE_PORT | Port number to bind the interface |
| ENV  | APP_KEY_PATH | The path to the file that contains the private key to use for this app |
| ENV  | APP_NAME | A recognisable name to tag any output from the app |
| ENV  | APP_DB_USERNAME | The name of username to authenticate with the postgres dbms  |
| ENV  | APP_DB_PASSWORD | The password to authenticate with the postgres dbms |
| ENV  | APP_DB_HOST | The host address of the postgres dbms |
| ENV  | APP_DB_PORT | The host port of the postgres dbms |
| ENV  | APP_DB_NAME | The name of the database to use |

## Account Entity

| Field          | Data Type  | Comments |
| -------------- | :--------: | -------- |
| id             | INTEGER    | Primary key |
| balance        | INTEGER    | A balance associated with this account |
| currency       | TEXT       | The currency associated with the balance that is associated with this account |
| attributes     | OBJECT     | A collection of key value pairs associated with this account |
| signature      | TEXT       | A signature that is used to verify the integrity of the record's balance, its currency and its attributes |
| dt_created     | BIGINTEGER | Epoch timestamp representing the datetime when this account was created |
| dt_updated     | BIGINTEGER / NULL | Epoch timestamp representing the datetime when this account was last updated |

## Transaction Entity

| Field             | Data Type  | Comments |
| ----------------- | :--------: | -------- |
| id                | INTEGER    | Primary key |
| tags              | ARRAY      | A collection of arbitrary tags to associate with this transaction |
| sent_amount       | INTEGER    | The amount that was sent during this transaction |
| sent_currency     | TEXT       | The currency associated with the amount that was sent during this transaction |
| received_amount   | INTEGER    | The amount that was received during this transaction |
| received_currency | TEXT       | The currency associated with the amount that was received during this transaction |
| sender            | INTEGER    | The primary key of the account that sent the money in this transaction |
| receiver          | INTEGER    | The primary key of the account that received the money in this transaction |
| attributes        | OBJECT     | A collection of key value pairs associated with this transaction |
| signature         | TEXT       | A signature that is used to verify the integrity of the record's amounts, currencies, accounts, and its attributes |
| dt_created        | BIGINTEGER | Epoch timestamp representing the datetime when this group was created |


## Operations

### Create account

`POST /accounts`

|  Parameter     | Location  | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| currency       | BODY      | TEXT       | The currency associated with the balance that is associated with this account |
| attributes     | BODY      | OBJECT     | The collection of key value pairs associated with this account |

### Get accounts

`GET /accounts`

|  Parameter     |  Location | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| offset         | QUERY     | INTEGER    | The number of records to skip |
| limit          | QUERY     | INTEGER    | The maximum number of records to return |
| [ids]          | QUERY     | ARRAY      | The collection of ids of the specific accounts to retrieve |

### Update account

`PUT /accounts/{id}`

|  Parameter   | Location  | Data Type  | Comments |
| ------------ | :-------: | :--------: | -------- |
| attributes   | BODY      | OBJECT     | The attributes to associate with this identity |

### Update account currency

`PUT /accounts/{id}/currency`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| currency         | BODY      | TEXT       | The currency to assign to this account and to use to convert their existing balance |
| rateBase         | BODY      | INTEGER    | The base of the exchange rate to use  to perform the transaction |
| rateExponent     | BODY      | INTEGER    | The exponent of the exchange rate to use to perform the transaction |
| tags             | BODY      | ARRAY      | The collection of arbitrary tags to associate with the transaction |
| [attributes]     | BODY      | OBJECT     | The collection of key value pairs to associate with this transaction |

### Delete accounts

`DELETE /accounts`

|  Parameter    | Location  | Data Type     | Comments |
| --------------| :-------: | :-----------: | -------- |
| ids           | QUERY     | ARRAY         | The collection of ids of the specific accounts to remove |

### Transfer funds

`POST /transactions`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| amount           | BODY      | INTEGER    | The amount to remove from the sender and to add to the receiver after applying the appropriate currency conversion |
| sender           | BODY      | INTEGER    | The primary key of the account to remove the funds from |
| receiver         | BODY      | INTEGER    | The primary key of the account to add the funds to |
| tags             | BODY      | ARRAY      | The collection of arbitrary tags to associate with the transaction |
| [rateBase]       | BODY      | INTEGER    | The base of the exchange rate to use  to perform the transaction if the account's currencies are different |
| [rateExponent]   | BODY      | INTEGER    | The exponent of the exchange rate to use to perform the transaction if the account's currencies are different |
| [attributes]     | BODY      | OBJECT     | The collection of key value pairs to associate with this transaction |

### Load funds

`POST /transactions/load`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| amount           | BODY      | INTEGER    | The amount to remove from the sender and to add to the receiver after applying the appropriate currency conversion |
| receiver         | BODY      | INTEGER    | The primary key of the account to add the funds to |
| tags             | BODY      | ARRAY      | The collection of arbitrary tags to associate with the transaction |
| [attributes]     | BODY      | OBJECT     | The collection of key value pairs to associate with this transaction |

### Unload funds

`POST /transactions/unload`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| amount           | BODY      | INTEGER    | The amount to remove from the sender and to add to the receiver after applying the appropriate currency conversion |
| sender           | BODY      | INTEGER    | The primary key of the account to remove the funds from |
| tags             | BODY      | ARRAY      | The collection of arbitrary tags to associate with the transaction |
| [attributes]     | BODY      | OBJECT     | The collection of key value pairs to associate with this transaction |

### Get transactions

`GET /transactions`

|  Parameter     |  Location | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| offset         | QUERY     | INTEGER    | The number of records to skip |
| limit          | QUERY     | INTEGER    | The maximum number of records to return |
| [tags]         | QUERY     | ARRAY      | The collection of arbitrary tags that should be associated with the transaction records that you want to retrieve |
| [sender]       | QUERY     | INTEGER    | The primary key of the sender that is associated with the transactions that you want to retrieve |
| [receiver]     | QUERY     | INTEGER    | The primary key of the receiver that is associated with the transactions that you want to retrieve |
| [ids]          | QUERY     | ARRAY      | The collection of ids of the specific transactions that you want to retrieve |
| [dt_from]      | QUERY     | INTEGER    | The epoch timestamp that represents the earliest datetime that should be associated with the transactions that you want to retrieve |
| [dt_to]        | QUERY     | INTEGER    | The epoch timestamp that represents the latest datetime that should be associated with the transactions that you want to retrieve |
